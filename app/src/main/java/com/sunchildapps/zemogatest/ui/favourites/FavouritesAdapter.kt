package com.sunchildapps.zemogatest.ui.favourites

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sunchildapps.zemogatest.databinding.ItemFavouriteBinding
import com.sunchildapps.zemogatest.domain.Post

class FavouritesAdapter: ListAdapter<Post, FavouritesAdapter.FavouritesPostsViewHolder>(
    FavouritePostsDiffCallback()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouritesPostsViewHolder {
        return FavouritesPostsViewHolder(
            ItemFavouriteBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: FavouritesPostsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class FavouritesPostsViewHolder(private val binding: ItemFavouriteBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Post) {
            binding.post = item
            binding.executePendingBindings()
        }
    }

    class FavouritePostsDiffCallback : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem == newItem
        }
    }
}