package com.sunchildapps.zemogatest.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.sunchildapps.zemogatest.R
import com.sunchildapps.zemogatest.di.Injector
import com.sunchildapps.zemogatest.workers.PostsDatabaseWorker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val factory = Injector.providesMainViewModel(this)
        viewModel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        initViews()
    }

    private fun initViews() {
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        view_pager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(view_pager)

        delete_fab.setOnClickListener {
            deleteAllPosts()
        }

        iv_refresh.setOnClickListener {
            refresh()
        }
    }

    private fun deleteAllPosts() {
        viewModel.deleteAllPosts()
    }

    private fun refresh() {
        deleteAllPosts()
        val request = OneTimeWorkRequestBuilder<PostsDatabaseWorker>().build()
        WorkManager.getInstance(this).enqueue(request)
    }
}