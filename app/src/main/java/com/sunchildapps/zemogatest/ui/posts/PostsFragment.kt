package com.sunchildapps.zemogatest.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sunchildapps.zemogatest.databinding.FragmentPostsBinding
import com.sunchildapps.zemogatest.di.Injector
import com.sunchildapps.zemogatest.domain.Post
import com.sunchildapps.zemogatest.utils.SwipeToDeleteCallback
import com.sunchildapps.zemogatest.utils.addDivider

class PostsFragment : Fragment() {

    private lateinit var viewModel: PostsViewModel
    private lateinit var binding: FragmentPostsBinding
    private val postsAdapter = PostsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        createViewModel()
        binding = FragmentPostsBinding.inflate(inflater, container, false)
        bindViews()

        addSwipeToDelete()
        return binding.root
    }

    private fun createViewModel() {
        val factory = Injector.providesPostsViewModel(requireContext())
        viewModel = ViewModelProvider(this, factory).get(PostsViewModel::class.java)
        viewModel.posts.observe(viewLifecycleOwner, ::update)
    }

    private fun bindViews() {
        binding.rvPosts.apply {
            adapter = postsAdapter
            layoutManager = LinearLayoutManager(context)
            addDivider()
        }
    }

    private fun addSwipeToDelete() {
        val swipeHandler = object : SwipeToDeleteCallback() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.posts.value?.get(viewHolder.adapterPosition)?.id?.let {
                    viewModel.deletePost(it)
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(binding.rvPosts)
    }

    private fun update(postsList: List<Post>) {
        postsAdapter.submitList(postsList)
    }

    companion object {
        fun newInstance() = PostsFragment()
    }
}