package com.sunchildapps.zemogatest.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.sunchildapps.zemogatest.data.repository.posts.PostsRepository
import kotlinx.coroutines.launch

class MainViewModel(
    private val localRepo: PostsRepository.Local
) : ViewModel() {

    fun deleteAllPosts() =
        viewModelScope.launch {
            localRepo.deleteAllPosts()
        }
}

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(
    private val localRepo: PostsRepository.Local
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(
                localRepo
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}