package com.sunchildapps.zemogatest.ui.details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.sunchildapps.zemogatest.R
import com.sunchildapps.zemogatest.databinding.ActivityDetailsBinding
import com.sunchildapps.zemogatest.di.Injector
import com.sunchildapps.zemogatest.domain.Post
import com.sunchildapps.zemogatest.utils.addDivider
import com.sunchildapps.zemogatest.workers.PostsDatabaseWorker
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailsBinding
    private lateinit var viewModel: DetailsViewModel

    private val commentsAdapter = CommentsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        binding.lifecycleOwner = (this)

        bindAdapter()

        bindPost()

        bindComments()

        bindToolbar()

        setPostAsRead()
    }

    private fun bindAdapter() {
        binding.apply {
            rvComments.apply {
                adapter = commentsAdapter
                layoutManager = LinearLayoutManager(this@DetailsActivity)
                addDivider()
            }
        }
    }

    private fun bindPost() {
        intent?.let {
            val extraPost = it.getIntExtra(EXTRA_POST_ID, DEFAULT)
            val extraUserId = it.getIntExtra(EXTRA_USER_ID, DEFAULT)
            val factory = Injector.providesDetailsViewModel(
                this,
                extraPost, extraUserId
            )
            viewModel = ViewModelProvider(this, factory).get(DetailsViewModel::class.java)
            binding.detailsViewodel = viewModel
        }
    }

    private fun bindComments() {
        viewModel.comments.observe(this) {
            if (it.isEmpty())
                viewModel.fetchComments()
            else
                commentsAdapter.submitList(it)
        }
    }

    private fun bindToolbar() {
        viewModel.post.observe(this) {
            toolbar.setNavigationOnClickListener { view ->
                super.onBackPressed()
            }
            toolbar.menu.getItem(MENU_FAVOURITES_POSITION).icon =
                renderFavouriteIcon(it.isFavourite)

            toolbar.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.add_to_fav_icon -> {
                        addOrRemovePostFromFavourites(it)
                        item.icon = renderFavouriteIcon(it.isFavourite)
                        true
                    }
                    else -> false
                }
            }
        }
    }

    private fun addOrRemovePostFromFavourites(post: Post) {
        if (post.isFavourite)
            viewModel.removePostFromFavourites()
        else
            viewModel.addPostToFavourites()
    }

    private fun renderFavouriteIcon(isFavourite: Boolean): Drawable? {
        return if (isFavourite)
            ContextCompat.getDrawable(this, DRAWABLE_ADDED_TO_FAVOURITES)
        else
            ContextCompat.getDrawable(this, DRAWABLE_REMOVED_FROM_FAVOURITES)
    }

    private fun setPostAsRead() {
        viewModel.markPostAsRead()
    }

    companion object {
        const val EXTRA_POST_ID = "extra_post_id"
        const val EXTRA_USER_ID = "extra_user_id"
        const val DEFAULT = 0
        const val MENU_FAVOURITES_POSITION = 0
        const val DRAWABLE_ADDED_TO_FAVOURITES = R.drawable.ic_baseline_star_24
        const val DRAWABLE_REMOVED_FROM_FAVOURITES = R.drawable.ic_baseline_star_border_24
    }
}