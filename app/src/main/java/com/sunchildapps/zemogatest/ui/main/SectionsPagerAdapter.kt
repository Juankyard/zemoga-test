package com.sunchildapps.zemogatest.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.sunchildapps.zemogatest.R
import com.sunchildapps.zemogatest.ui.favourites.FavouritesFragment
import com.sunchildapps.zemogatest.ui.posts.PostsFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_posts,
    R.string.tab_favourites
)

private val tabFragmentInstanceDelegate = listOf(
   PostsFragment.newInstance(),
   FavouritesFragment.newInstance()
)

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return tabFragmentInstanceDelegate[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return tabFragmentInstanceDelegate.size
    }
}