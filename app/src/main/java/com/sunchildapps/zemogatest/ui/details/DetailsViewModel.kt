package com.sunchildapps.zemogatest.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.sunchildapps.zemogatest.data.repository.comments.CommentsRepository
import com.sunchildapps.zemogatest.data.repository.posts.PostsRepository
import com.sunchildapps.zemogatest.data.repository.users.UsersRepository
import kotlinx.coroutines.launch

class DetailsViewModel(
    private val usersRepository: UsersRepository.Local,
    private val postRepo: PostsRepository.Local,
    private val commentsRepo: CommentsRepository.Local,
    private val commentsRepoRemote: CommentsRepository.Remote,
    private val postId: Int,
    private val userId: Int
) : ViewModel() {
    val post = postRepo.getPostById(postId)
    val user = usersRepository.getUserById(userId)
    val comments = commentsRepo.getCommentsByPostId(postId) as MutableLiveData

    fun addPostToFavourites() {
        viewModelScope.launch {
            postRepo.addPostToFavourites(postId)
        }
    }

    fun removePostFromFavourites() {
        viewModelScope.launch {
            postRepo.removePostFromFavourites(postId)
        }
    }

    fun markPostAsRead() {
        viewModelScope.launch {
            postRepo.setPostAsRead(postId)
        }
    }

    fun fetchComments() {
        viewModelScope.launch {
            val commentsResponse = commentsRepoRemote.fetchCommentsByPostId(postId)
            commentsRepo.insertComments(commentsResponse)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class DetailsViewModelFactory(
    private val usersRepository: UsersRepository.Local,
    private val repository: PostsRepository.Local,
    private val commentsRepo: CommentsRepository.Local,
    private val commentsRepoRemote: CommentsRepository.Remote,
    private val postId: Int,
    private val userId: Int
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(
                usersRepository,
                repository,
                commentsRepo,
                commentsRepoRemote,
                postId,
                userId
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}