package com.sunchildapps.zemogatest.ui.posts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.sunchildapps.zemogatest.data.repository.posts.PostsRepository
import kotlinx.coroutines.launch

class PostsViewModel(
    private val repository: PostsRepository.Local
) : ViewModel() {

    private val _posts = repository.getPosts()

    val posts
    get() = _posts

    fun  deletePost(postId: Int) {
        viewModelScope.launch {
            repository.deletePost(postId)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class PostsViewModelFactory(val repository: PostsRepository.Local): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostsViewModel::class.java)) {
            return PostsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}