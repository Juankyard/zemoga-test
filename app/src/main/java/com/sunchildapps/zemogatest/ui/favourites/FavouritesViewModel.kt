package com.sunchildapps.zemogatest.ui.favourites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sunchildapps.zemogatest.data.repository.posts.PostsRepository

class FavouritesViewModel(
    val repository: PostsRepository.Local
) : ViewModel() {
    private val _favourites = repository.getFavouritePosts()
    val favourites
    get() = _favourites
}

@Suppress("UNCHECKED_CAST")
class FavouritesViewModelFactory(val repository: PostsRepository.Local): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavouritesViewModel::class.java)) {
            return FavouritesViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}