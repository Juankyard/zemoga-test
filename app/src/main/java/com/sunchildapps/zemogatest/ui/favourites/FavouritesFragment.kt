package com.sunchildapps.zemogatest.ui.favourites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.sunchildapps.zemogatest.databinding.FragmentFavouritesBinding
import com.sunchildapps.zemogatest.di.Injector
import com.sunchildapps.zemogatest.domain.Post
import com.sunchildapps.zemogatest.utils.addDivider

class FavouritesFragment : Fragment() {

    private lateinit var binding: FragmentFavouritesBinding
    private lateinit var viewModel: FavouritesViewModel

    private val favAdapter = FavouritesAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavouritesBinding.inflate(
            inflater,
            container,
            false
        )
        createViewModel()
        bindViews()
        return binding.root
    }

    private fun createViewModel() {
        val factory = Injector.providesFavouritesViewModel(requireContext())
        viewModel = ViewModelProvider(this, factory).get(FavouritesViewModel::class.java)
        viewModel.favourites.observe(viewLifecycleOwner, ::update)
    }

    private fun bindViews() {
        binding.rvFavourites.apply {
            adapter = favAdapter
            layoutManager = LinearLayoutManager(context)
            addDivider()
        }
    }

    private fun update(postsList: List<Post>) {
        favAdapter.submitList(postsList)
    }

    companion object {
        fun newInstance() = FavouritesFragment()
    }

}