package com.sunchildapps.zemogatest.ui.posts

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sunchildapps.zemogatest.databinding.ItemPostsBinding
import com.sunchildapps.zemogatest.domain.Post
import com.sunchildapps.zemogatest.ui.details.DetailsActivity
import com.sunchildapps.zemogatest.ui.details.DetailsActivity.Companion.EXTRA_POST_ID
import com.sunchildapps.zemogatest.ui.details.DetailsActivity.Companion.EXTRA_USER_ID

class PostsAdapter : ListAdapter<Post, PostsAdapter.PostsViewHolder>(PostsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        return PostsViewHolder(
            ItemPostsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PostsViewHolder(private val binding: ItemPostsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Post) {
            binding.apply {
                post = item
                setOnClick {
                    onClick()
                }
                executePendingBindings()

            }
        }

        private fun onClick() {
            binding.post?.let {
                val intent = Intent(binding.root.context, DetailsActivity::class.java)
                intent.putExtra(EXTRA_POST_ID,it.id)
                intent.putExtra(EXTRA_USER_ID, it.userId)
                binding.root.context.startActivity(intent)
            }
        }
    }

    class PostsDiffCallback : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem == newItem
        }
    }
}

