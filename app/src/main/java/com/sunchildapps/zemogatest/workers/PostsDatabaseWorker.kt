package com.sunchildapps.zemogatest.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.sunchildapps.zemogatest.data.db.PostsDataBase
import com.sunchildapps.zemogatest.data.mappers.CommentsMapper
import com.sunchildapps.zemogatest.data.mappers.PostsMapper
import com.sunchildapps.zemogatest.data.mappers.UsersMapper
import com.sunchildapps.zemogatest.data.network.RetrofitClient
import com.sunchildapps.zemogatest.data.repository.comments.CommentsRemoteRepositoryImpl
import com.sunchildapps.zemogatest.data.repository.posts.PostsRemoteRepositoryImpl
import com.sunchildapps.zemogatest.data.repository.users.UsersRemoteRepositoryImpl
import com.sunchildapps.zemogatest.utils.SimPosts.getReadPosts
import com.sunchildapps.zemogatest.utils.SimPosts.getUnreadPosts

class PostsDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result {
        return try {

            val postsResponse = PostsRemoteRepositoryImpl(
                RetrofitClient.createService(),
                PostsMapper()
            ).fetchPosts()

            val usersResponse = UsersRemoteRepositoryImpl(
                RetrofitClient.createService(),
                UsersMapper()
            ).fetchUsers()

            val database = PostsDataBase.getInstance(applicationContext)
            database.postsDao().insertPosts(getUnreadPosts(postsResponse))
            database.postsDao().insertPosts(getReadPosts(postsResponse))

            database.usersDao().insertUsers(usersResponse)

            Result.success()
        } catch (ex: Exception) {
            Log.e(TAG, "Error posts database", ex)
            Result.failure()
        }
    }

    companion object {
        private const val TAG = "PostsDatabaseWorker"
    }
}
