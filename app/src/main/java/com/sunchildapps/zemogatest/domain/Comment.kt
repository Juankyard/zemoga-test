package com.sunchildapps.zemogatest.domain

class Comment(
    val id: Int,
    val postId: Int,
    val body: String
) {
}