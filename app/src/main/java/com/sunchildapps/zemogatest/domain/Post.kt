package com.sunchildapps.zemogatest.domain

data class Post(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String,
    val isFavourite: Boolean = false,
    val isPostRead: Boolean = false
)