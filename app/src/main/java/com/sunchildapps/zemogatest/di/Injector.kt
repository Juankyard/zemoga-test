package com.sunchildapps.zemogatest.di

import android.content.Context
import com.sunchildapps.zemogatest.data.db.PostsDataBase
import com.sunchildapps.zemogatest.data.mappers.CommentsMapper
import com.sunchildapps.zemogatest.data.mappers.PostsMapper
import com.sunchildapps.zemogatest.data.mappers.UsersMapper
import com.sunchildapps.zemogatest.data.network.RetrofitClient
import com.sunchildapps.zemogatest.data.repository.comments.CommentsLocalRepositoryImpl
import com.sunchildapps.zemogatest.data.repository.comments.CommentsRemoteRepositoryImpl
import com.sunchildapps.zemogatest.data.repository.posts.PostsLocalRepositoryImpl
import com.sunchildapps.zemogatest.data.repository.posts.PostsRemoteRepositoryImpl
import com.sunchildapps.zemogatest.data.repository.users.UsersLocalRepositoryImpl
import com.sunchildapps.zemogatest.ui.details.DetailsViewModelFactory
import com.sunchildapps.zemogatest.ui.favourites.FavouritesViewModelFactory
import com.sunchildapps.zemogatest.ui.main.MainViewModelFactory
import com.sunchildapps.zemogatest.ui.posts.PostsViewModelFactory

object Injector {

    private fun providePostsLocalRepository(context: Context) =
        PostsLocalRepositoryImpl.getInstance(
            PostsDataBase.getInstance(context.applicationContext).postsDao(),
            PostsMapper()
        )

    private fun providesPostRemoteRepository(context: Context) =
        PostsRemoteRepositoryImpl(
            RetrofitClient.createService(),
            PostsMapper()
        )

    private fun provideUserLocalRepository(context: Context) = UsersLocalRepositoryImpl(
        PostsDataBase.getInstance(context.applicationContext).usersDao(),
        UsersMapper()
    )

    private fun providesCommentsLocalRepository(context: Context) = CommentsLocalRepositoryImpl(
        PostsDataBase.getInstance(context.applicationContext).commentsDao(),
        CommentsMapper()
    )

    private fun providesCommentsRemoteRepository() = CommentsRemoteRepositoryImpl(
        RetrofitClient.createService(),
        CommentsMapper()
    )

    fun providesPostsViewModel(context: Context) =
        PostsViewModelFactory(providePostsLocalRepository(context))

    fun providesFavouritesViewModel(context: Context) =
        FavouritesViewModelFactory(providePostsLocalRepository(context))

    fun providesDetailsViewModel(context: Context, postId: Int, userId: Int) =
        DetailsViewModelFactory(
            provideUserLocalRepository(context),
            providePostsLocalRepository(context),
            providesCommentsLocalRepository(context),
            providesCommentsRemoteRepository(),
            postId,
            userId
        )

    fun providesMainViewModel(context: Context) =
        MainViewModelFactory(
            providePostsLocalRepository(context)
        )
}