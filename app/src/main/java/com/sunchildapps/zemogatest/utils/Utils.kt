package com.sunchildapps.zemogatest.utils

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.sunchildapps.zemogatest.data.db.entities.PostEntity

fun RecyclerView.addDivider() {
    addItemDecoration(DividerItemDecoration(this.context, RecyclerView.VERTICAL))
}

abstract class SwipeToDeleteCallback :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }
}

object SimPosts {
    private const val SIMULATED_POSTS_QTY = 20
    fun getUnreadPosts(posts: List<PostEntity>): List<PostEntity> {
        return posts.toMutableList().take(SIMULATED_POSTS_QTY).map {
            it.apply {
                isPostRead = false
            }
        }
    }

    fun getReadPosts(serviceRepose: List<PostEntity>): List<PostEntity> {
        return serviceRepose.toMutableList().subList(SIMULATED_POSTS_QTY, serviceRepose.size).map {
            it.apply {
                it.isPostRead = true

            }
        }
    }
}