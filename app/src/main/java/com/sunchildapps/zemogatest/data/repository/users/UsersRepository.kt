package com.sunchildapps.zemogatest.data.repository.users

import androidx.lifecycle.LiveData
import com.sunchildapps.zemogatest.data.db.entities.UsersEntity
import com.sunchildapps.zemogatest.domain.User

interface UsersRepository {

    interface Local {
        fun getUserById(userId: Int): LiveData<User>
    }

    interface  Remote {
        suspend fun fetchUsers() : List<UsersEntity>
    }
}