package com.sunchildapps.zemogatest.data.network

import com.sunchildapps.zemogatest.data.network.response.CommentResponse
import com.sunchildapps.zemogatest.data.network.response.PostResponse
import com.sunchildapps.zemogatest.data.network.response.UserResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface PostsApi {

    @GET("/posts")
    suspend fun getPosts(): List<PostResponse>

    @GET("/comments")
    suspend fun getCommentsByPostId(@Query("postId") postId: Int): List<CommentResponse>

    @GET("/users")
    suspend fun getUsers(): List<UserResponse>
}