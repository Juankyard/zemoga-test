package com.sunchildapps.zemogatest.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.sunchildapps.zemogatest.data.db.TableNames.COMMENTS_TABLE
import com.sunchildapps.zemogatest.data.db.entities.CommentsEntity

@Dao
interface CommentsDao {

    @Query("SELECT * FROM $COMMENTS_TABLE WHERE postId = :postId")
    fun getCommentsByPostId(postId: Int):LiveData<List<CommentsEntity>>

    @Insert(onConflict = IGNORE)
    fun insertComments(commentsList: List<CommentsEntity>)
}