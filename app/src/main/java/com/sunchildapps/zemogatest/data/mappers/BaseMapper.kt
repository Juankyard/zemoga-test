package com.sunchildapps.zemogatest.data.mappers

interface BaseMapper<R, E, M> {
    fun transformResponseToEntity(response: R): E
    fun transformEntityToModel(entity: E): M
}