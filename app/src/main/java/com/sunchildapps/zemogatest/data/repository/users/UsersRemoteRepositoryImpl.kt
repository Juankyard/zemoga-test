package com.sunchildapps.zemogatest.data.repository.users

import com.sunchildapps.zemogatest.data.db.entities.UsersEntity
import com.sunchildapps.zemogatest.data.mappers.UsersMapper
import com.sunchildapps.zemogatest.data.network.PostsApi

class UsersRemoteRepositoryImpl(
    private val postsApi: PostsApi,
    private val mapper: UsersMapper
): UsersRepository.Remote {
    override suspend fun fetchUsers(): List<UsersEntity> {
        return postsApi.getUsers().map {
            mapper.transformResponseToEntity(it)
        }
    }
}