package com.sunchildapps.zemogatest.data.repository.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.sunchildapps.zemogatest.data.db.dao.UsersDao
import com.sunchildapps.zemogatest.data.db.entities.UsersEntity
import com.sunchildapps.zemogatest.data.mappers.UsersMapper
import com.sunchildapps.zemogatest.domain.User

class UsersLocalRepositoryImpl(
    private val usersDao: UsersDao,
    private val mapper: UsersMapper
): UsersRepository.Local {

    fun insertUsers(users: List<UsersEntity>){
        usersDao.insertUsers(users)
    }

    override fun getUserById(userId: Int): LiveData<User> {
        return Transformations.map(usersDao.getUserById(userId)) { entity ->
            mapper.transformEntityToModel(entity)
        }
    }

    companion object {
        @Volatile
        private var instance: UsersLocalRepositoryImpl? = null

        fun getInstance(usersDao: UsersDao, mapper: UsersMapper) =
            instance ?: synchronized(this) {
                instance
                    ?: UsersLocalRepositoryImpl(
                        usersDao,
                        mapper
                    ).also { instance = it }
            }
    }
}