package com.sunchildapps.zemogatest.data.repository.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.sunchildapps.zemogatest.data.db.dao.PostsDao
import com.sunchildapps.zemogatest.data.db.entities.PostEntity
import com.sunchildapps.zemogatest.data.mappers.PostsMapper
import com.sunchildapps.zemogatest.domain.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PostsLocalRepositoryImpl(
    private val postsDao: PostsDao,
    private val mapper: PostsMapper
) : PostsRepository.Local {

    fun insertPosts(posts: List<PostEntity>){
        postsDao.insertPosts(posts)
    }

    override fun getPosts(): LiveData<List<Post>> {
        return Transformations.map(postsDao.getPosts()) { entities ->
            entities.map {
                mapper.transformEntityToModel(it)
            }
        }
    }

    override fun getPostById(postId: Int): LiveData<Post> {
        return Transformations.map(postsDao.getPostById(postId)) {
            mapper.transformEntityToModel(it)
        }
    }

    override suspend fun deletePost(postId: Int) = withContext(Dispatchers.IO) {
        postsDao.deletePostById(postId)
    }

    override suspend fun deleteAllPosts() = withContext(Dispatchers.IO) {
        postsDao.deleteAllPosts()
    }

    override suspend fun addPostToFavourites(postId: Int) = withContext(Dispatchers.IO) {
        postsDao.addPostToFavourites(postId)
    }

    override suspend fun removePostFromFavourites(postId: Int) = withContext(Dispatchers.IO) {
        postsDao.removePostFromFavourites(postId)
    }

    override fun getFavouritePosts(): LiveData<List<Post>> {
        return Transformations.map(postsDao.getFavouritePosts()) { entities ->
            entities.map {
                mapper.transformEntityToModel(it)
            }
        }
    }

    override suspend fun setPostAsRead(postId: Int) = withContext(Dispatchers.IO) {
        postsDao.setPostAsRead(postId)
    }

    companion object {
        @Volatile
        private var instance: PostsLocalRepositoryImpl? = null

        fun getInstance(postDao: PostsDao, mapper: PostsMapper) =
            instance ?: synchronized(this) {
                instance
                    ?: PostsLocalRepositoryImpl(
                        postDao,
                        mapper
                    ).also { instance = it }
            }
    }
}