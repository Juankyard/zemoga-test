package com.sunchildapps.zemogatest.data.repository.comments

import com.sunchildapps.zemogatest.data.db.entities.CommentsEntity
import com.sunchildapps.zemogatest.data.mappers.CommentsMapper
import com.sunchildapps.zemogatest.data.network.PostsApi

class CommentsRemoteRepositoryImpl(
    private val api:PostsApi,
    private val mapper: CommentsMapper
): CommentsRepository.Remote {
    override suspend fun fetchCommentsByPostId(postId: Int): List<CommentsEntity> {
        return api.getCommentsByPostId(postId).map {
            mapper.transformResponseToEntity(it)
        }
    }
}