package com.sunchildapps.zemogatest.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.sunchildapps.zemogatest.data.db.dao.CommentsDao
import com.sunchildapps.zemogatest.data.db.dao.PostsDao
import com.sunchildapps.zemogatest.data.db.dao.UsersDao
import com.sunchildapps.zemogatest.data.db.entities.CommentsEntity
import com.sunchildapps.zemogatest.data.db.entities.PostEntity
import com.sunchildapps.zemogatest.data.db.entities.UsersEntity
import com.sunchildapps.zemogatest.workers.PostsDatabaseWorker

private const val DATABASE_NAME = "PostsDatabase"

@Database(entities = [PostEntity::class, CommentsEntity::class, UsersEntity::class], version = 1)
abstract class PostsDataBase : RoomDatabase() {

    abstract fun postsDao(): PostsDao
    abstract fun usersDao(): UsersDao
    abstract fun commentsDao(): CommentsDao

    companion object {

        @Volatile
        private var instance: PostsDataBase? = null

        fun getInstance(context: Context): PostsDataBase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): PostsDataBase {
            return Room.databaseBuilder(context, PostsDataBase::class.java, DATABASE_NAME)
                .addCallback(
                    object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            val request = OneTimeWorkRequestBuilder<PostsDatabaseWorker>().build()
                            WorkManager.getInstance(context).enqueue(request)
                        }
                    }
                )
                .build()
        }
    }
}

object TableNames {
    const val USERS_TABLE = "users_table"
    const val COMMENTS_TABLE = "comments_table"
    const val POSTS_TABLE = "posts_table"
}