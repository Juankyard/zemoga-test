package com.sunchildapps.zemogatest.data.mappers

import com.sunchildapps.zemogatest.data.db.entities.CommentsEntity
import com.sunchildapps.zemogatest.data.db.entities.PostEntity
import com.sunchildapps.zemogatest.data.db.entities.UsersEntity
import com.sunchildapps.zemogatest.data.network.response.CommentResponse
import com.sunchildapps.zemogatest.data.network.response.PostResponse
import com.sunchildapps.zemogatest.data.network.response.UserResponse
import com.sunchildapps.zemogatest.domain.Comment
import com.sunchildapps.zemogatest.domain.Post
import com.sunchildapps.zemogatest.domain.User

class PostsMapper : BaseMapper<PostResponse, PostEntity, Post>{
    override fun transformResponseToEntity(response: PostResponse): PostEntity {
        return PostEntity(
            response.postId,
            response.userId,
            response.title,
            response.body,
            isFavourite = false,
            isPostRead = false
        )
    }

    override fun transformEntityToModel(entity: PostEntity): Post {
        return Post(
            entity.postId,
            entity.userId,
            entity.title,
            entity.body,
            entity.isFavourite,
            entity.isPostRead
        )
    }
}

class UsersMapper : BaseMapper<UserResponse, UsersEntity, User> {
    override fun transformResponseToEntity(response: UserResponse): UsersEntity {
        return UsersEntity(
            response.userId,
            response.name,
            response.email,
            response.phone,
            response.website
        )
    }

    override fun transformEntityToModel(entity: UsersEntity): User {
        return User(
            entity.userId,
            entity.name,
            entity.email,
            entity.phone,
            entity.website
        )
    }

}

class CommentsMapper : BaseMapper<CommentResponse, CommentsEntity, Comment> {
    override fun transformResponseToEntity(response: CommentResponse): CommentsEntity {
       return CommentsEntity(
           response.commentId,
           response.postId,
           response.body
       )
    }

    override fun transformEntityToModel(entity: CommentsEntity): Comment {
        return Comment(
            entity.commentId,
            entity.postId,
            entity.body
        )
    }

}