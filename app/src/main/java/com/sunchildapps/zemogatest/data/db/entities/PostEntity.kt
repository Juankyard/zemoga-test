package com.sunchildapps.zemogatest.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.sunchildapps.zemogatest.data.db.TableNames.POSTS_TABLE

@Entity(tableName = POSTS_TABLE)
data class PostEntity(
    @SerializedName("id")
    @PrimaryKey
    val postId: Int,
    val userId: Int,
    val title: String,
    val body: String,
    val isFavourite: Boolean,
    var isPostRead: Boolean
)