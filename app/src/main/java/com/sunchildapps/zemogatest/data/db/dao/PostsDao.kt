package com.sunchildapps.zemogatest.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sunchildapps.zemogatest.data.db.TableNames.POSTS_TABLE
import com.sunchildapps.zemogatest.data.db.entities.PostEntity

private const val TRUE = 1
private const val FALSE = 0

@Dao
interface PostsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPosts(posts: List<PostEntity>)

    @Query("SELECT * FROM $POSTS_TABLE")
    fun getPosts(): LiveData<List<PostEntity>>

    @Query("SELECT * FROM $POSTS_TABLE WHERE postId = :postId")
    fun getPostById(postId: Int): LiveData<PostEntity>

    @Query("DELETE FROM $POSTS_TABLE")
    fun deleteAllPosts()

    @Query("DELETE FROM $POSTS_TABLE WHERE postId = :postId")
    fun deletePostById(postId: Int)

    @Query("SELECT * FROM $POSTS_TABLE WHERE isFavourite = $TRUE")
    fun getFavouritePosts(): LiveData<List<PostEntity>>

    @Query("UPDATE $POSTS_TABLE SET isFavourite = $TRUE WHERE postId= :postId")
    fun addPostToFavourites(postId: Int)

    @Query("UPDATE $POSTS_TABLE SET isFavourite = $FALSE WHERE postId= :postId")
    fun removePostFromFavourites(postId: Int)

    @Query("UPDATE $POSTS_TABLE SET isPostRead = $TRUE WHERE postId= :postId")
    fun setPostAsRead(postId: Int)

}