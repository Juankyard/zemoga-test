package com.sunchildapps.zemogatest.data.network.response

import com.google.gson.annotations.SerializedName

class CommentResponse (
    val postId: Int,
    @SerializedName("id")val commentId: Int,
    val name: String,
    val body: String
)