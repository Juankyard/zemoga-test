package com.sunchildapps.zemogatest.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sunchildapps.zemogatest.data.db.TableNames.USERS_TABLE
import com.sunchildapps.zemogatest.data.db.entities.UsersEntity

@Dao
interface UsersDao {

    @Query("SELECT * FROM $USERS_TABLE WHERE userId=:userId")
    fun getUserById(userId: Int): LiveData<UsersEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUsers(usersList: List<UsersEntity>)
}