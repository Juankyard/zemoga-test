package com.sunchildapps.zemogatest.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.sunchildapps.zemogatest.data.db.TableNames.USERS_TABLE

@Entity(tableName = USERS_TABLE)
data class UsersEntity(
    @SerializedName("id")
    @PrimaryKey val userId: Int,
    val name: String,
    val email: String,
    val phone: String,
    val website: String
)