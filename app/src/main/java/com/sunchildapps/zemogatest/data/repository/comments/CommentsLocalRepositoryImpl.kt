package com.sunchildapps.zemogatest.data.repository.comments

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.sunchildapps.zemogatest.data.db.dao.CommentsDao
import com.sunchildapps.zemogatest.data.db.dao.UsersDao
import com.sunchildapps.zemogatest.data.db.entities.CommentsEntity
import com.sunchildapps.zemogatest.data.mappers.CommentsMapper
import com.sunchildapps.zemogatest.data.mappers.UsersMapper
import com.sunchildapps.zemogatest.data.repository.users.UsersLocalRepositoryImpl
import com.sunchildapps.zemogatest.domain.Comment
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class CommentsLocalRepositoryImpl(
    private val commentsDao: CommentsDao,
    private val mapper: CommentsMapper
): CommentsRepository.Local {

    override suspend fun insertComments(comments: List<CommentsEntity>){
        withContext(IO) {
            commentsDao.insertComments(comments)
        }
    }

    override fun getCommentsByPostId(postId: Int): LiveData<List<Comment>> {
       return Transformations.map(commentsDao.getCommentsByPostId(postId)){ entities ->
           entities.map {
               mapper.transformEntityToModel(it)
           }
       }
    }

    companion object {
        @Volatile
        private var instance: CommentsLocalRepositoryImpl? = null

        fun getInstance(commentsDao: CommentsDao, mapper: CommentsMapper) =
            instance ?: synchronized(this) {
                instance
                    ?: CommentsLocalRepositoryImpl(
                        commentsDao,
                        mapper
                    ).also { instance = it }
            }
    }

}