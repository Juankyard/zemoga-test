package com.sunchildapps.zemogatest.data.repository.posts

import androidx.lifecycle.LiveData
import com.sunchildapps.zemogatest.data.db.entities.PostEntity
import com.sunchildapps.zemogatest.domain.Post
import org.koin.core.KoinComponent

interface PostsRepository {

    interface Local {
        fun getPosts(): LiveData<List<Post>>

        fun getPostById(postId: Int): LiveData<Post>

        suspend fun deletePost(postId: Int)

        suspend fun deleteAllPosts()

        suspend fun addPostToFavourites(postId: Int)

        suspend fun removePostFromFavourites(postId: Int)

        fun getFavouritePosts() : LiveData<List<Post>>

        suspend fun setPostAsRead(postId: Int)
    }

    interface Remote {
        suspend fun fetchPosts(): List<PostEntity>
    }
}