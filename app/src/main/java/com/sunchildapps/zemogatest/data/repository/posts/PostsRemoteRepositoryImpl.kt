package com.sunchildapps.zemogatest.data.repository.posts

import com.sunchildapps.zemogatest.data.db.entities.PostEntity
import com.sunchildapps.zemogatest.data.mappers.PostsMapper
import com.sunchildapps.zemogatest.data.network.PostsApi

class PostsRemoteRepositoryImpl(
    private val api: PostsApi,
    private val mapper: PostsMapper
) : PostsRepository.Remote {
    override suspend fun fetchPosts(): List<PostEntity> {
        return api.getPosts().map {
            mapper.transformResponseToEntity(it)
        }
    }
}