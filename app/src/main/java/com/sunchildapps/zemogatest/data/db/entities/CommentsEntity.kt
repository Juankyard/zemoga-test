package com.sunchildapps.zemogatest.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.sunchildapps.zemogatest.data.db.TableNames.COMMENTS_TABLE

@Entity(tableName = COMMENTS_TABLE)
data class CommentsEntity(
    @SerializedName("id")
    @PrimaryKey val commentId: Int,
    val postId: Int,
    val body: String
)