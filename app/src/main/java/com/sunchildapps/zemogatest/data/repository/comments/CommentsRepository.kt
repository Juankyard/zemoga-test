package com.sunchildapps.zemogatest.data.repository.comments

import androidx.lifecycle.LiveData
import com.sunchildapps.zemogatest.data.db.entities.CommentsEntity
import com.sunchildapps.zemogatest.domain.Comment

interface CommentsRepository {

    interface Local {
        suspend fun insertComments(comments: List<CommentsEntity>)
        fun getCommentsByPostId(postId: Int): LiveData<List<Comment>>
    }

    interface Remote {
        suspend fun fetchCommentsByPostId(postId: Int): List<CommentsEntity>
    }
}