# ZemogaTest

[![Build Status](https://travis-ci.org/adsf117/Clean_Post_AAC.svg?branch=develop)](https://travis-ci.org/adsf117/Clean_Post_AAC.svg?branch=develop)

This is a demonstration Android project, created as an example of implementation of different libraries, and clean architecture.

This project simulates an inbox, by consuming a REST API from [JSON Placeholder](https://jsonplaceholder.typicode.com/)

It follows the MVVM architecture pattern, and uses  [Android Architeture Components](https://developer.android.com/topic/libraries/architecture) like Room, LiveData, ViewModel, etc.


# Features ##

* Execute the project on an Android device or emulator, and provide Internet connection to it.
* You will see a pager where you can find an inbox style posts list tab, adn a favourites tab.
* You can see each post detail by tapping on it.
* On the details screen, you can add it to your favourite posts by pressing the star located in the upper right corner (if the post is already set as favourite, a full white star is drawn, and you can delete it from your favourites)
* You can swipe to the right to delete a single post
* You can delete all posts by pressing the floating action button on the lower right
* You can refresh by pressing the refresh icon on the upper right corner.

# TODO ##
* UnitTests

## Libraries Used :

* [Retrofit2](https://square.github.io/retrofit/)
* [Room](https://developer.android.com/jetpack/androidx/releases/room)
* [Coroutines](https://developer.android.com/kotlin/coroutines)
* [Workout manager](https://developer.android.com/topic/libraries/architecture/workmanager/advanced/coroutineworker)
* [Navigation Component](https://codelabs.developers.google.com/codelabs/android-navigation/index.html?index=..%2F..index#0)
* [Data Binding](https://codelabs.developers.google.com/codelabs/android-databinding/index.html?index=..%2F..index#5) 

